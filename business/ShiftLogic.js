const Shifts = require('../firebase/Shifts');
const Jobs = require('../firebase/Jobs');

const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
];
  
const daysInMonth = 30;

  const GenerateShift = (job, day) => {
    const dayOfWeek = day.getDay();
    const dayOfWeekString = days[dayOfWeek];
    if (job.daysOfWeek.includes(dayOfWeekString)) {
        const shiftStartList = job.shiftStart.split(':');
        const startMinutes = parseInt(shiftStartList[1]);
        const startHours = parseInt(shiftStartList[0]);
        const startShift = startHours + (startMinutes / 60);
  
        const shiftEndList = job.shiftEnd.split(':');
        const endMinutes = parseInt(shiftEndList[1]);
        const endShift = parseInt(shiftEndList[0]) + (endMinutes / 60);
  
        const duration = endShift - startShift;
  
        day.setHours(startHours);
        day.setMinutes(startMinutes);
        day.setSeconds(0);
  
        const shift = {
            caregiverName: job.caregiverName,
            start: day,
            duration: duration,
            clientId: job.clientId,
            caregiverId: job.caregiverId,
            status: 'scheduled'
        };
  
        console.log('generated shift:', shift);
        
        return shift;
      }
  };

const GenerateShiftsForOneDay = async (db) => {
    const clients = await db.collection('users')
    .where('role', '==', 'client')
    .get();
  
    clients.forEach(async (doc) => {
      const clientId = doc.id;
  
      const jobs = await Jobs.GetAcceptedJobs(db, clientId);
        jobs.forEach(async (job) => {
            const date = new Date();
            const day = date.addDays(daysInMonth);
            const shift = GenerateShift(job, day);
            if (shift) {
                await Shifts.AddShift(db, shift);    
            }
        });
    });
};
  
const GenerateShifts = async (db, job) => {
    const date = new Date();

    for(let i = 0; i <= daysInMonth; i++) {
      const day = date.addDays(i);

      const shift = GenerateShift(job, day);
      
      if (shift) {
          await Shifts.AddShift(db, shift);
      }
    }    
};

const ClassifyShifts = async (db) => {
    const now = new Date();

    const activeShifts = await db.collection('shifts')
    .where('start', '<', now)
    .where('status', '==', 'scheduled')
    .get();

    activeShifts.forEach(async (shift) => {
        const data = {
            id: shift.id,
            ...shift.data()
        };
        await Shifts.MarkAsActive(db, data);
    });

    const markedActiveShifts = await db.collection('shifts')
    .where('status', '==', 'active')
    .get();

    markedActiveShifts.forEach(async (shift) => {
        const data = {
            id: shift.id,
            ...shift.data()
        };
        const seconds = data.start.seconds;
        const duration = data.duration; // duration hours
        const endSeconds = seconds + (duration * 60 * 60);
        const endTime = new Date(endSeconds * 1000);
        if (endTime < now) {
            await Shifts.MarkAsPast(db, data);
        }
    });
};

const CancelShift = async (db, shift) => {
    await Shifts.MarkAsCancelled(db, shift);
};

module.exports = {
    GenerateShiftsForOneDay,
    GenerateShifts,
    ClassifyShifts,
    CancelShift
};

Date.prototype.addDays = function(dias) {
    var date = new Date(this.valueOf());
    date.setDate(parseInt(date.getDate()) + parseInt(dias));
    return date;
};

Date.prototype.addHours = function(horas) {
    var date = new Date(this.valueOf());
    date.setHours(parseInt(date.getHours()) + parseInt(horas));
    return date;
};