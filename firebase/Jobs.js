const GetAcceptedClientJobs = async (db, clientId) => {
    return await db.collection('jobs')
    .where('status', '==', 'accepted')
    .where('clientId', '==', clientId)
    .get()
    .then((query) => {
      return query.docs.map((doc) => {
          return {
            id: doc.id,
            ...doc.data()
          };
      });
    });
};

  const GetAcceptedJob = async (db, clientId, caregiverId) => {
    return await db.collection('jobs')
    .where('status', '==', 'accepted')
    .where('clientId', '==', clientId)
    .where('caregiverId', '==', caregiverId)
    .get()
    .then((query) => {
      return query.docs.map((doc) => {
          return {
            id: doc.id,
            ...doc.data()
          };
      });
    });
  };

  const GetAcceptedJobs = async (db) => {
    return await db.collection('jobs')
    .where('status', '==', 'accepted')
    .get()
    .then((query) => {
      return query.docs.map((doc) => {
          return {
            id: doc.id,
            ...doc.data()
          };
      });
    });
};

const AcceptJob = async (db, job) => {
  let jobDoc = await db.collection('jobs').doc(job.id).get();

  if (jobDoc.exists) {
      let jobRef = jobDoc.ref;
      jobRef.update({
          status: 'accepted'
      });
  }
};

exports.AcceptJob = AcceptJob;
exports.GetAcceptedJobs = GetAcceptedJobs;
exports.GetAcceptedClientJobs = GetAcceptedClientJobs;
exports.GetAcceptedJob = GetAcceptedJob;