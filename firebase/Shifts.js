const AddShift = async (db, shift) => {
    await db.collection('shifts').add(shift);
};

const MarkAsPast = async (db, shift) => {
    let shiftDoc = await db.collection('shifts')
    .doc(shift.id)
    .get();

    if (shiftDoc.exists) {
        let shiftRef = shiftDoc.ref;
        shiftRef.update({
            status: 'past'
        });
    }
};

const MarkAsActive = async (db, shift) => {
    let shiftDoc = await db.collection('shifts')
    .doc(shift.id)
    .get();

    if (shiftDoc.exists) {
        let shiftRef = shiftDoc.ref;
        shiftRef.update({
            status: 'active'
        });
    }
};

const MarkAsCancelled = async (db, shift) => {
    let shiftDoc = await db.collection('shifts')
    .doc(shift.id)
    .get();

    if (shiftDoc.exists) {
        let shiftRef = shiftDoc.ref;
        shiftRef.update({
            status: 'cancelled'
        });
    }
};

const GetShift = async (db, shift) => {
    return await db.collection('shifts').doc(shift.id).get();
};

exports.AddShift = AddShift;
exports.MarkAsPast = MarkAsPast;
exports.MarkAsActive = MarkAsActive;
exports.MarkAsCancelled = MarkAsCancelled;
exports.GetShift = GetShift;