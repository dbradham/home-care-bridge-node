'use strict';

const express = require('express');
const uuid = require('uuid/v4');
const scheduler = require('node-schedule');
const admin = require('firebase-admin');
const shiftLogic = require('./business/ShiftLogic');
const jobs = require('./firebase/Jobs');
const shifts = require('./firebase/Shifts');

const serviceAccount = {
  "type": "service_account",
  "project_id": "carebridge-f0a8d",
  "private_key_id": "1df50bd6ab9f34b9685ae5f9eac49a2a38dae650",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCvyCnV4ml0PAb4\nLqenVyUzAsinibJs1sEbskjINZy9mxpqlFoYqYyde0iJaupCOgZIeQ5re/2tvOq3\n3cmpT0JQNBjAx2CrR2KwPLT0WB93zcU3mnRARirwEPYYRdZbWeQ8h0zDT3+5NGi1\n7Hb1UaKky1kBwxi3phxpYp+XN8i0AUxEfBz0c3fVydd/eYoiB9eajtkGuhGDpnmW\nvbByo1aat1mnHn0dh96kZXkITDKWWk97/jBYWZzgxEopp8R0b5YJdcWPXCCCpnbZ\ncctFmaMTzzguTNVHLtlc5E22M/cx1Jn5lHj2jah+abwb+wxdtgJyoB8hCEp36rBH\nHC7d/7WDAgMBAAECggEAMkatVIvv/ntGwQhTEmU18gubSXP0UPXxgJLzBzRyUrbA\njPhGdqRT4B0JR6iJPQzsjCD3s11g1qbgeVtZmO/e9yKXa9f6ubJusxqB5RspUPJN\nNJScKGAJWuV3BcU/xJwG0BsgpQIhdV7cjiAGfC7vtnOkzwC2mpxLpwa6v50Ibhcw\nHgP1VJ7bOrbOPiVvJxW1++ZUsdg4X6k1ZqV/Nh5TtIjeSwmpqZQ5Sov1cuVJiEu3\n4Em3SRRpFUqvZ4yPhWjLi6wFusZrRjL3JC6CzmOE+uZkYOhespQs9aH4mSdZnvwo\njbp23QCfQ8VffDWc7c9bdSAs8F9t/PcZelsC+EJ9WQKBgQD38SUr3WgTPo5TOmVe\nQwzI43G5AZyy0OSIQfUz2yq3CcSbBwJX6uR0Rr3fqWIvODzRY0cD/Y4SNy4QDKn2\nj11VfJOcWCFD3WcUvSDDqsb8Ad5CQA6e4jnhrAp1wKF+o5Vuq2AUIWfgZT2L5DF4\ndyioYljGk41A49hqjrm0yX69mwKBgQC1fqcgSCkbRTZ9ykNDvmjoy08FWETC85u8\nRj8OCLCkK62UUrbjFg4aAJqTrzkOVVZBx2DDJC08uV09NBRivx68Toepd77DiO0C\n3MDQsKLRz9Pr3i93yxFXE4hzE98lc/dZ/OG9CDup84K3vtKxLtfnUJpatblIWxW9\n+UcVJ81aOQKBgA9i7J6bP9Ep6aUqk43U9V8mFjGX8wR5yCTYiwHr0OvI9GV2euO/\n1OuOVEwj887gW43W7lpGCuIAgKBIWOfPH8ilUXe7VF0kyTfVQMpDt9N9vjA4T2/2\ntAbYKVIDYUI9npFemzjElfCr97TNtXA3XNOOPtob4AEt4olCUC3y7yWPAoGAO/FJ\np1rN4MjaN29vCdfAdGp5fc1qYGzu2Z7lwm/ST1ZxrNQovRWSsLnUU8SCdp5ur6j2\n51xyaGTcS5OAzCBNOWpCWhFHevMlPFXGhOZh0CPw3Szzxn/WCUlVPpSolYkYf6sK\nr1Ehy362hJNMqOJMwoQXhrAQMrqKksTa2jzcENkCgYEA42p7BMz0cOBZdsD08P2D\nWfnwfGUPl/sYG3z7XIkTjTlNpay3IEWw2AWnuEhjZ1+a0LSmxLM+qYj+RTi/IiFx\neBW4BC3uPc4V6pQohsHtXAQ3G9j41jQU1gvLEUmgZxKU9sQ+S84OJYQXMQ912dbR\nC5wqOYUvFzOUnifYEm4sONU=\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-ro5kq@carebridge-f0a8d.iam.gserviceaccount.com",
  "client_id": "114141204098044975833",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-ro5kq%40carebridge-f0a8d.iam.gserviceaccount.com"
};

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://carebridge-f0a8d.firebaseio.com"
});

const db = admin.firestore();

const app = express();

app.use(express.json());

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', req.header('origin')
  || req.header('x-forwarded-host') 
  || req.header('referer') 
  || req.header('host'));

  // res.setHeader('Access-Control-Allow-Methods', 'POST, OPTIONS');

  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

  next();
});

app.get('/', (req, res) => {
  res
    .status(200)
    .send(`Hello, ${uuid()}!`)
    .end();
});

app.post('/accept-job', (req, res) => {
  const job = req.body;
  jobs.AcceptJob(db, job);
  shiftLogic.GenerateShifts(db, job);

  res.status(200)
  .send('Okay')
  .end();
});

app.post('/cancel-shift', (req, res) => {
  const shift = req.body;
  shiftLogic.CancelShift(db, shift)
  .then(result => {
    // send a notification to the other party!
    
    res.status(200)
    .send('Okay')
    .end();
  })
  .catch(error => {
    res.status(500)
    .send(error)
    .end();
  });
});

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {

  const shiftCreationJobRule = new scheduler.RecurrenceRule();
  shiftCreationJobRule.minute = 1;
  shiftCreationJobRule.hour = 0;

  const shiftCreationJob = scheduler
  .scheduleJob(shiftCreationJobRule, createShifts);

  const cleanShiftsJobRule = new scheduler.RecurrenceRule();
  cleanShiftsJobRule.minute = [new scheduler.Range(0, 45, 15)];

  const cleanShiftsJob = scheduler
  .scheduleJob(cleanShiftsJobRule, cleanShifts);  
});

const createShifts = async function() {
  try {
    console.log("Starting to generate shifts for 30 days from now.");
    await shiftLogic.GenerateShiftsForOneDay(db);
    console.log("Finished generating shifts for 30 days from now.");
  } catch (e) {
    console.log('error creating shifts:', e);
  }
};

const cleanShifts = async function() {
  try {
    console.log('Starting to clean shifts');
    await shiftLogic.ClassifyShifts(db);
    console.log('Finished cleaning shifts');
  } catch (e) {
    console.log('error cleaning shifts:', e);
  }
};